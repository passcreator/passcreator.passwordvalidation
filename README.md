# Passcreator.PasswordValidation

This package provides a Validator that checks if a password has been exposed in a data leak using https://haveibeenpwned.com/.
The password is anonymized by only sending the first five characters of the MD5 hash of the password to the haveibeenpwned API and then checking if the result contains the complete hash.
That way the actual password is never sent to any external service.
The package is actively used on Passcreator and there's a [blog post](https://www.passcreator.com/en/blog/how-we-verify-if-a-password-you-enter-has-been-compromised.html) that explains how we use it.

Details can be found [here](https://haveibeenpwned.com/API/v3).

## Setup

To install the package, use composer.

```
composer require passcreator/passwordvalidation
```

## Usage

Just add the HaveIBeenPwnedValidator to your model or action.
E.g. for an action:
```
    /**
     * Creates a new user
     * @Flow\Validate(argumentName="password", type="\Passcreator\PasswordValidation\Validation\Validator\HaveIBeenPwnedValidator")
     *
     * @param string                     $username
     * @param string                     $password
     *
     * @return void
     */
    public function createUserAction(string $username, string $password) {}
```
