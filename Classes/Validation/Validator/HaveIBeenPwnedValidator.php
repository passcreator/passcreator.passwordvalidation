<?php

namespace Passcreator\PasswordValidation\Validation\Validator;

use DivineOmega\PasswordExposed\Enums\PasswordStatus;
use Neos\Flow\Annotations as Flow;

/**
 * Validator that checks if the password is on the have i been pwned list (https://haveibeenpwned.com/)
 *
 * @Flow\Scope("singleton")
 */
class HaveIBeenPwnedValidator extends \Neos\Flow\Validation\Validator\AbstractValidator {

    /**
     * Check if $value is an exposed password.
     *
     * @param mixed $value
     *
     * @return void
     */
    protected function isValid($value) {
        if (password_exposed($value) === PasswordStatus::EXPOSED) {
            // only do the haveibeenpwned check if the password matches the criteria. Otherwise the user won't be able to use it anway.
            $this->addError('This password has been exposed in a previous hack or data leak. Please change it wherever you use it and choose a different one.',
                1576484689);
        }
    }

}

?>
